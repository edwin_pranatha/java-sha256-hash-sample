FROM openjdk:8-jdk-alpine
VOLUME /tmp
ADD java-project-1.0-SNAPSHOT.jar /app/java-project-1.0-SNAPSHOT.jar
#CMD ["java","-jar","/app/java-project-1.0-SNAPSHOT.jar"]
ENTRYPOINT ["java", "-jar", "/app/java-project-1.0-SNAPSHOT.jar"]